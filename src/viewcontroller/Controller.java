package viewcontroller;

import connectmodel.*;

import viewcontroller.View;

public class Controller {
	
	private GameBoard myGameBoard;
	private GameEngine myGameEngine;
	private View myView;
	private Player myPlayer;
	
	
	
	public Controller()
	{
        myPlayer = new Player("Jesus", PieceType.YELLOW);
        myGameBoard = new GameBoard(6,7,4,PieceType.values());
        myGameEngine = new GameEngine(myPlayer, myGameBoard);
        myView = new View(this);
        myGameEngine.startGame();
	}
	
	public String getPlayerName()
	{
		String playername;
		playername = myGameEngine.getPlayers().firstElement().getName();
		return playername;
	}

	public String getCpuName()
	{
		String CPUname; 
		CPUname = myGameEngine.getPlayers().lastElement().getName();
		return CPUname;
	}
	
	public void resetBoard()
	{
		myGameEngine.getGameBoard().resetBoard();
		myView = new View(this);
	}
	
	public void placePiece(Integer iCol)
	{
		int row, column;
		column = iCol.intValue();
		myGameEngine.placePiece(iCol);
		row = myGameBoard.getmyLastPoint().y;
		
		myView.changeImage(column, row, PieceType.YELLOW);
		myGameEngine.switchPlayerUp();
		placeCPUPiece();
	}
	
	public void placeCPUPiece()
	{
		int row, column;
		column = myGameBoard.findBestMoveColumn(PieceType.BLACK);
		myGameEngine.placePiece(column);
		row = myGameBoard.getmyLastPoint().y;
		
		myView.changeImage(column, row, PieceType.BLACK);
		myGameEngine.switchPlayerUp();
		
	}
	
	public Player getPlayerUp()
	{
		return myGameEngine.getPlayerUp();
	}
	
	
	public String getScoreValueCPU()
    {
        String scoreCPU;
        
        scoreCPU = ""+myGameEngine.getPlayers().lastElement().getScore();
        return scoreCPU;
    }

	public String getScoreValuePlayer()
    {
        String scoreCPU;
        
        scoreCPU = ""+myGameEngine.getPlayers().firstElement().getScore();
        return scoreCPU;
    }
	
	public GameEngine getGameEngine()
	{
		return myGameEngine;
	}
	
	public Player getPlayer()
	{
		return myPlayer;
	}
}
