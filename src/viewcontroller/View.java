package viewcontroller;

import java.awt.*;
import java.lang.reflect.Method;

import connectmodel.PieceType;
import viewcontroller.ButtonListener;
import viewcontroller.Controller;

@SuppressWarnings("serial")
public class View extends Frame 
{
	public final static int myNumColumns = 7;
	public final static int myNumRows = 6;
	private Can[][] myCanvas;
	private Panel myGameBoardPanel;
	private ButtonListener[] myBoardListener;
	private ButtonListener myStartGameListener;
	private Button myButtonNewGame;
	private Label myTextScore;
	private Label myTextP1Name;
	private Label myTextP2Name;
	private Label myTextScoreDivisor;
	private Label myTextP1Score;
	private Label myTextP2Score;

	private Controller myController;
	private Image myP1Image;
	private Image myP2Image;
	private Image myBlankImage;
	
	public View(Controller controller)
	{

		
		this.setSize(800,800);
		this.setLayout(null);
		this.setBackground(Color.cyan);
		
		
		myBoardListener = new ButtonListener[myNumColumns];
		myP1Image = Toolkit.getDefaultToolkit().getImage("images/BluePiece.png");
		myP2Image = Toolkit.getDefaultToolkit().getImage("images/YellowPiece.png");
		myBlankImage = Toolkit.getDefaultToolkit().getImage("images/blank.jpg");
		
		
		myCanvas = new Can[myNumRows][myNumColumns];
		myGameBoardPanel = new Panel(new GridLayout(myNumRows,myNumColumns));
		myGameBoardPanel.setSize(600,500);
		myGameBoardPanel.setLocation(100,150);
		
		for(int i = 0; i < myNumRows; i++)
		{
			for(int j = 0; j <myNumColumns; j++)
			{
				myCanvas[i][j] = new Can(myBlankImage);
				myGameBoardPanel.add(myCanvas[i][j]);
			}
		}
		
		
		myController = controller;
		
		myButtonNewGame = new Button("New Game");
		myButtonNewGame.setSize(100, 20);
		myButtonNewGame.setLocation(650, 650);
		
		
		myTextP1Name = new Label(myController.getPlayerName());
		myTextP1Name.setSize(50,20);
		myTextP1Name.setLocation(100, 80);
		
		myTextP2Name = new Label(myController.getCpuName());
		myTextP2Name.setSize(100,20);
		myTextP2Name.setLocation(650, 80);
		
		myTextScoreDivisor = new Label(":");
		myTextScoreDivisor.setLocation(393,80);
		myTextScoreDivisor.setSize(10,12);
		
		myTextScore = new Label("SCORE");
		myTextScore.setLocation(375,50);
		myTextScore.setSize(50,20);

		myTextP1Score = new Label(myController.getScoreValueCPU());
		myTextP1Score.setLocation(370,80);
		myTextP1Score.setSize(10,12);
		
		myTextP2Score = new Label(myController.getScoreValuePlayer());
		myTextP2Score.setLocation(410,80);
		myTextP2Score.setSize(10,12);

		
		this.add(myGameBoardPanel);
		this.add(myButtonNewGame);
		this.add(myTextP1Name);
		this.add(myTextP2Name);
		this.add(myTextScore);
		this.add(myTextScoreDivisor);
		this.add(myTextP1Score);
		this.add(myTextP2Score);
		this.setVisible(true);
		this.addWindowListener(new AWindowListener());
		this.associateListeners();
	}
	
	public void associateListeners()
    {
        Class<? extends Controller> controllerClass;
        Method placepieceMethod;
        Method resetBoardMethod;
        Class<?>[] classArgs;

        controllerClass = myController.getClass();
        
        placepieceMethod = null;
        resetBoardMethod = null;
        classArgs = new Class[1];
        
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException e)
        {
           String error;
           error = e.toString();
           System.out.println(error);
        }
        
        
        try
        {
        	placepieceMethod = controllerClass.getMethod("placePiece",classArgs);      
        }
        catch(NoSuchMethodException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        
        try
        {
        	resetBoardMethod = controllerClass.getMethod("resetBoard",classArgs);      
        }
        catch(NoSuchMethodException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        
        int i;
        int j;
        Integer[] args;
        
        for (i=0; i < myNumColumns; i++)
        {
        	args = new Integer[1];
            args[0] = new Integer(i);
            myBoardListener[i] = 
                    new ButtonListener(myController, placepieceMethod, args);
        	for(j = 0 ; j < myNumRows; j++)
        	{
        		
                myCanvas[j][i].addMouseListener(myBoardListener[i]);
        	}
        }
    } 
	
	public void changeImage(int col, int row, PieceType type)
    {
		Image i;
		if(type == PieceType.YELLOW)
		{
			i = myP1Image;
		}
		else if(type == PieceType.BLACK)
		{
			i = myP2Image;
		}
		else
		{
			i = myBlankImage;
		}
		myCanvas[row][col].setImage(i);
    }
	

}
